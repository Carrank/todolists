class Profile < ActiveRecord::Base
	belongs_to :user
  validates :first_name, presence: true, unless: "last_name.present?"
  validates :last_name, presence: true, unless: "first_name.present?"
  validates :gender, inclusion: %w(male female)

  validate :notsue

  def notsue
  	if gender == "male" && first_name == "Sue"
  		errors.add(:base, "A male cannot be named Sue")
  	end
  end
  def self.get_all_profiles(min, max)
  	Profile.where("birth_year BETWEEN :min_year and :max_year", min_year: min, max_year: max).order("birth_year ASC")
  end
end
